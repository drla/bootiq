<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\BranchService;
use App\Transport\TransportCompanyFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class BranchController extends AbstractController
{
    private BranchService $branchService;

    public function __construct(BranchService $branchService)
    {
        $this->branchService = $branchService;
    }

    public function getApiBranches(): JsonResponse
    {
        $data = $this->branchService->getAllBranches(TransportCompanyFactory::TRANSPORT_COMPANY_1);

        if (empty($data)) {
            $data = ['error' => 'Branches not found'];
        }

        return new JsonResponse($data);
    }

    public function getApiBranch(int $id): JsonResponse
    {
        $data = $this->branchService->getBranchById(TransportCompanyFactory::TRANSPORT_COMPANY_1, $id);

        if (empty($data)) {
            $data = ['error' => 'Branch not found'];
        }

        return new JsonResponse($data);
    }
}