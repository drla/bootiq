<?php

declare(strict_types=1);

namespace App\Model;

class BranchModel implements \JsonSerializable
{
    /**
     * @var string
     */
    private string $internalId;

    /** @var string */
    private string $internalName;

    /** @var Coordinates */
    private Coordinates $location;

    /** @var BusinessHourModel[] */
    private array $businessHours;

    /** @var string */
    private string $address;

    /** @var string */
    private string $web;

    /** @var string */
    private string $announcement;

    public function __construct(
        ?string $internalId,
        ?string $internalName,
        ?Coordinates $location,
        ?array $businessHours,
        ?string $address,
        ?string $web,
        ?string $announcement
    ) {
        $this->internalId = $internalId;
        $this->internalName = $internalName;
        $this->location = $location;
        $this->businessHours = $businessHours;
        $this->address = $address;
        $this->web = $web;
        $this->announcement = $announcement;
    }

    public function jsonSerialize(): array
    {
        return [
            "internalId" => $this->getInternalId(),
            "internalName" => $this->getInternalName(),
            "location" => $this->getLocation(),
            "businessHours" => $this->getBusinessHours(),
            "address" => $this->getAddress(),
            "web" => $this->getWeb(),
            "announcement" => $this->getAnnouncement()
        ];
    }

    /**
     * @return string
     */
    public function getInternalId(): ?string
    {
        return $this->internalId;
    }

    /**
     * @param string|null $internalId
     */
    public function setInternalId(?string $internalId): void
    {
        $this->internalId = $internalId;
    }

    /**
     * @return string
     */
    public function getInternalName(): ?string
    {
        return $this->internalName;
    }

    /**
     * @param string|null $internalName
     */
    public function setInternalName(?string $internalName): void
    {
        $this->internalName = $internalName;
    }

    /**
     * @return Coordinates
     */
    public function getLocation(): ?Coordinates
    {
        return $this->location;
    }

    /**
     * @param Coordinates|null $location
     */
    public function setLocation(?Coordinates $location): void
    {
        $this->location = $location;
    }

    /**
     * @return BusinessHourModel[]
     */
    public function getBusinessHours(): ?array
    {
        return $this->businessHours;
    }

    /**
     * @param BusinessHourModel[] $businessHours
     */
    public function setBusinessHours(?array $businessHours): void
    {
        $this->businessHours = $businessHours;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getWeb(): ?string
    {
        return $this->web;
    }

    /**
     * @param string|null $web
     */
    public function setWeb(?string $web): void
    {
        $this->web = $web;
    }

    /**
     * @return string
     */
    public function getAnnouncement(): ?string
    {
        return $this->announcement;
    }

    /**
     * @param string|null $announcement
     */
    public function setAnnouncement(?string $announcement): void
    {
        $this->announcement = $announcement;
    }
}
