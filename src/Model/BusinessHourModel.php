<?php

declare(strict_types=1);

namespace App\Model;

class BusinessHourModel implements \JsonSerializable
{
    /** @var string */
    private string $dayOfWeek;

    /** @var string */
    private string $businessHour;

    public function __construct(string $dayOfWeek, string $businessHour)
    {
        $this->dayOfWeek = $dayOfWeek;
        $this->businessHour = $businessHour;
    }

    public function jsonSerialize(): array
    {
        return [
            "dayOfWeek" => $this->getDayOfWeek(),
            "businessHour" => $this->getBusinessHour()
        ];
    }

    /**
     * @return string
     */
    public function getDayOfWeek(): string
    {
        return $this->dayOfWeek;
    }

    /**
     * @param string $dayOfWeek
     */
    public function setDayOfWeek(string $dayOfWeek): void
    {
        $this->dayOfWeek = $dayOfWeek;
    }

    /**
     * @return string
     */
    public function getBusinessHour(): string
    {
        return $this->businessHour;
    }

    /**
     * @param string $businessHour
     */
    public function setBusinessHour(string $businessHour): void
    {
        $this->businessHour = $businessHour;
    }
}
