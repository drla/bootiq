<?php

declare(strict_types=1);

namespace App\Model;

class Coordinates implements \JsonSerializable
{
    /**
     * @var float
     */
    private float $lat;
    /**
     * @var float
     */
    private float $lng;

    public function __construct(float $lat, float $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng): void
    {
        $this->lng = $lng;
    }

    public function jsonSerialize(): array
    {
        return [
            "lat" => $this->getLat(),
            "lng" => $this->getLng()
        ];
    }
}