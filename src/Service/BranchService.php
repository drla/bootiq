<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\BranchModel;
use App\Transport\TransportCompanyFactory;

class BranchService
{
    /**
     * @var TransportCompanyFactory
     */
    private TransportCompanyFactory $factory;

    public function __construct(TransportCompanyFactory $factory)
    {
        $this->factory = $factory;
    }
    /**
     * @return BranchModel[]
     */
    public function getAllBranches(string $company): array
    {
        $transportCompany = $this->factory->getTransportCompany($company);
        return $transportCompany->getAllBranches();
    }

    /**
     * @param string $company
     * @param int $id
     * @return BranchModel|null
     */
    public function getBranchById(string $company, int $id): ?BranchModel
    {
        $transportCompany = $this->factory->getTransportCompany($company);
        return $transportCompany->getBranchById($id);
    }
}