<?php

declare(strict_types=1);

namespace App\Transport;

use App\Model\BranchModel;
use App\Model\BusinessHourModel;
use App\Model\Coordinates;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class FirstTransport implements TransportInterface
{
    const ENDPOINT = 'https://www.ulozenka.cz/gmap';
    const HTTP_OK = 200;

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    /**
     * FirstTransport constructor.
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return array|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function apiCall(): ?array
    {
        $response = $this->client->request(
            'GET',
            self::ENDPOINT
        );

        $content = null;
        if ($response->getStatusCode() === self::HTTP_OK) {
            $content = $response->toArray();
        }

        return $content;
    }

    /**
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getAllBranches(): array
    {
        try {
            $branches = $this->apiCall();
        } catch (\Exception $e) {
            // log exception
            // $e->getMessage();
        }

        $result = [];
        foreach ($branches as $branch) {
            $result[] = $this->mapModel($branch);
        }

        return $result;
    }

    /**
     * @param $id
     * @return BranchModel|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getBranchById($id): ?BranchModel
    {
        try {
            $branches = $this->apiCall();
        } catch (\Exception $e) {
            // log exception
            // $e->getMessage();
        }

        foreach ($branches as $branch) {
            $model = $this->mapModel($branch);
            if ((int)$model->getInternalId() === $id) {
                return $model;
            }
        }

        return null;
    }

    /**
     * @param array $branch
     * @return BranchModel
     */
    private function mapModel(array $branch): BranchModel
    {
        $coordinates = array_key_exists('lat', $branch) && array_key_exists('lng', $branch)
            ? new Coordinates($branch['lat'], $branch['lng'])
            : null;

        $businessHours = array_map(function($day) {
            return new BusinessHourModel(
                (string)$day['day'],
                $day['open'] . ' - ' .$day['close']
            );
        }, $branch['openingHours']);

        return new BranchModel(
            array_key_exists('id', $branch) ? (string)$branch['id'] : null,
            array_key_exists('shortcut', $branch) ? $branch['shortcut'] : null,
            $coordinates,
            $businessHours,
            array_key_exists('name', $branch) ? $branch['name'] : null,
            array_key_exists('odkaz', $branch) ? $branch['odkaz'] : null,
            ''
        );
    }
}