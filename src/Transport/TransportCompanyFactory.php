<?php

declare(strict_types=1);

namespace App\Transport;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class TransportCompanyFactory
{
    const TRANSPORT_COMPANY_1 = 'Transport Company 1';
    const TRANSPORT_COMPANY_2 = 'Transport Company 2';

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $company
     * @return FirstTransport|SecondTransport
     */
    public function getTransportCompany(string $company)
    {
        switch ($company) {
            case self::TRANSPORT_COMPANY_1:
                return new FirstTransport($this->client);
                break;
            case self::TRANSPORT_COMPANY_2:
                return new SecondTransport($this->client);
                break;
        }
    }
}