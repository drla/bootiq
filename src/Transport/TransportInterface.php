<?php

declare(strict_types=1);

namespace App\Transport;

use App\Model\BranchModel;

interface TransportInterface
{
    public function getAllBranches(): array;
    public function getBranchById($id): ?BranchModel;
}